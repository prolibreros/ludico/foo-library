var type  = 'story',
    lang  = 'es',
    debug = false,
    timer

window.onload = function () {
    create()
}

function create() {
    foo.create({
        type            :   type,
        url             :   './json/' + type + '_' + lang + '.json',
        lang            :   lang,
        on_lose         :   function(){console.log('on_lose')},
        on_win          :   function(){console.log('on_win')},
        on_new_time     :   on_new_time,
        on_new_score    :   on_new_score,
        on_new_word     :   on_new_word,
        on_new_record   :   on_new_record
    })
}

function wipe () {
    foo.wipe('all')
    foo.destroy()
    setTimeout(create, 300)
}

function print (t) {
    var div_head = document.getElementById('foo-head-1')

    if (t == 'on_line_completed' || t == 'on_new_record' || t == 'on_new_word')
        console.log(t)
    else if (t == 'on_new_time')
        if (debug)
            console.log(t)
        else
            console.log(
                t,
                '\n  type: ',       foo.data.type,
                '\n  active: ',     foo.data.active,
                '\n  finished: ',   foo.data.finished,
                '\n  time: ',       foo.data.time,
                '\n  score: ',      foo.data.score,
                '\n  past_score: ', foo.data.past_score,
                '\n  line: ',       foo.data.line,
                '\n  letter: ',     foo.data.letter, 
                '\n  words: ',      foo.data.words,
                '\n  new_words: ',  foo.data.new_words
            )
    else {
        clearInterval(timer)
        div_head.innerHTML = '<p style="width:100%;height:100%;display:flex;align-items:center;justify-content:center;margin:0;padding:0;font-size:.5em;text-align:center;">' + t + '<br />more in console</p>'
    }

    timer = setInterval(function () {
        clearInterval(timer);
        if (document.getElementById('foo-head-1') != null)
            document.getElementById('foo-head-1').innerHTML = ''
    }, 3000)
}

function on_new_time        () {print('on_new_time'      )}
function on_new_score       () {print('on_new_score'     )}
function on_new_word        () {print('on_new_word'      )}
function on_new_record      () {print('on_new_record'    )}
function on_line_completed  () {print('on_line_completed')}
function on_line_correct    () {print('on_line_correct'  )}
function on_line_incorrect  () {print('on_line_incorrect')}
function on_first_change    () {print('on_first_change'  )}
function on_change          () {print('on_change'        )}
function on_loop            () {print('on_loop'          )}
function on_bomb            () {print('on_bomb'          )}
function on_bonus           () {print('on_bonus'         )}
