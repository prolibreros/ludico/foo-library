# JSON (archivo de salida creado por `create_jsons.rb`)

## Estructura del modo arcade

```
{
  "type": "arcade",
  "time": 60,
  "score": 0,
  "words": [
    Array
  ],
  "content": [
    Strings
  ]
}
```

* `type`. `String` que indica el tipo de juego: `arcade` o `historia`.
* `time`. `Integer` que indica el tiempo faltante (para el modo arcade) o transcurrido (para el modo historia).
* `score`. `Integer` que indica la puntuación actual.
* `words`. `Array` con todas las palabras únicas, ordenadas alfabéticamente y con versal inicial.

Consideraciones:

1. Por simplificación en modo arcade el contenido solo es una lista de 
   palabras como texto. 
2. La librería se encargará de darle la estructura adecuada, lo que 
   también posibilita que nunca será la misma.
3. Si se hará de manera manual, todas las líneas de texto tienen que
   tener la misma extensión. Por ejemplo, si la extensión es 7, 
   entonces «casa» tiene que escribirse como «casa» más 3 espacios. 
   Los espacios sirven para cumplir con la extensión.

## Estructura del modo historia

```
{
  "type": "story",
  "time": 0,
  "score": 0,
  "words": [
    Array
  ],
  "content": [
    Objects
  ]
}
```

Los objetos principales son similares a sus pares del modo arcade.

Las diferencias empiezan en el tipo de elemento contenido en el
`content`. En este caso se tratan objetos para cada una de las líneas 
con esta estructura:

```
{
  "visible": true,
  "on_line_completed": "on_line_completed",
  "on_line_correct": "on_line_correct",
  "on_line_incorrect": "on_line_incorrect",
  "content": [
    Objects
  ]
}
```

* `visible`. `Bool` que indica si la línea es visible.
* `on_line_completed`. `String` que señala la función a ejecutar si la línea se completó.
* `on_line_correct`. `String` que señala la función a ejecutar si la línea se completó correctamente.
* `on_line_incorrect`. `String` que señala la función a ejecutar si la línea se completó incorrectamente.
* `content`. `Array` que contiene cada una de las letras como un objeto.

Estos otros objetos son para cada una de las letras cuya estructura es
semejante a:

```
{
  "type": 1,
  "status": "enable",
  "correct": "A",
  "picked": "M",
  "picked_i": 0,
  "letters": [
    "M",
    "A"
  ],
  "chances": 3,
  "loops": 0,
  "on_first_change": "on_first_change",
  "on_change": "on_change",
  "on_loop": "on_loop",
  "on_bomb": "on_bomb",
  "on_bonus": "on_bonus"
}
```

* `type`. `Integer` que indica el tipo de elemento, `0` si es espacio o `1` para el resto. Ojo: los tipos `0` carecen del resto de las propiedades.
* `status`. `String` que indica el estado de la letra: `enable`, `disable`, `correct` o `incorrect`.
* `correct`. `String` que indica cuál es el caracter correcto.
* `picked`. `String` que indica el caracter elegido.
* `picked_i`. `Integer` que indica el índice del caracter elegido.
* `letters`. `Array` que indica las opciones de respuesta.
* `chances`. `Integer` que indica la cantidad de oportunidades
* `loops`. `Integer` que indica cuántas vueltas se han dado a todas las opciones de respuesta.
* `on_first_change`. `String` que señala la función a ejecutar al empezar a seleccionar la letra.
* `on_change`. `String` que señala la función a ejecutar al cambiar de opción.
* `on_loop`. `String` que señala la función a ejecutar si ya se recorrieron todas las opciones.
* `on_bomb`. `String` que señala la función a ejecutar si se descubre una bomba.
* `on_bonus`. `String` que señala la función a ejecutar si se descubre un bono.

En conjunto, la estructura se parece a:

```
{
  "type": "story",
  "time": 0,
  "score": 0,
  "content": [
    {
      "visible": true,
      "on_line_completed": "on_line_completed",
      "on_line_correct": "on_line_correct",
      "on_line_incorrect": "on_line_incorrect",
      "content": [
        {
          "type": 1,
          "status": "enable",
          "correct": "A",
          "picked": "M",
          "picked_i": 0,
          "letters": [
            "M",
            "A"
          ],
          "chances": 3,
          "loops": 0,
          "on_first_change": "on_first_change",
          "on_change": "on_change",
          "on_loop": "on_loop",
          "on_bomb": "on_bomb",
          "on_bonus": "on_bonus"
        }
      ]
    }
  ]
}
```

Consideraciones:

1. Las opciones de respuesta siempre son aleatorias y según su tipo
   (vocal, consonante o puntuación) y altura (versal o normal).
2. La creación por defecto de esta estructura hará que aumente la 
   cantidad de opciones y de bombas o bonos según se acerque al final 
   de la historia. Esto es para automatizar la subida de dificultad.
3. Por defecto todo el resalte tipográfico es falso. Hay que modificarlo 
   de manera manual.
4. Por defecto todas las funciones se llaman igual a la propiedad del 
   objeto. Hay que modificarlo de manera manual.
5. Por la complejidad de la estructura no se recomienda hacerla a mano.
   Lo ideal es crearla con el _script_ de Ruby y luego modificar a
   discreción.
