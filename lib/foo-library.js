// Objeto que incluye toda la lógica de Foo
var foo = (function() {
  'use strict';

  // Todo lo relativo al juego
  var game_opt;

  // Lenguaje
  var lang_loading = 'Loading…',
      lang_load_error = 'Data not found.';

  // Variables que van cambiando
  var touch_old = null,
      touch_timer,
      game_timer,
      bar_timer;

  // Obtiene una muestra aleatoria del conjunto
  Object.defineProperty(Array.prototype, 'sample', {
    enumerable: false,
    value: function() {
      return this[Math.floor(Math.random() * this.length)];
    }
  });

  // Limpia un conjunto
  Object.defineProperty(Array.prototype, 'clear', {
    enumerable: false,
    value: function() {
      return this.filter(function(e) {
        return e;
      });
    }
  });

  // Revuelve el conjunto; viene de: http://alturl.com/33yti
  Object.defineProperty(Array.prototype, 'shuffle', {
    enumerable: false,
    value: function() {
      var x = this.length, j, temp;

      if (x === 0) {
        return this;
      }

      while (--x) {
        j = Math.floor(Math.random() * (x + 1));
        temp = this[x];
        this[x] = this[j];
        this[j] = temp;
      }

      return this;
    }
  });

  // Capitaliza la línea de texto
  Object.defineProperty(String.prototype, 'capitalize', {
    enumerable: false,
    value: function() {
      return this.charAt(0).toUpperCase() +
        this.toLowerCase().slice(1);
    }
  });

  // Elimina tildes a las letras; basado en: http://alturl.com/37gbp
  Object.defineProperty(String.prototype, 'transliterate', {
    enumerable: false,
    value: function() {
      var translate_re = /[ÁÉÍÓÚÜÑáéíóúüñ]/g,
          translate = {
            'Á': 'A',
            'É': 'E',
            'Í': 'I',
            'Ó': 'O',
            'Ú': 'U',
            'Ü': 'U',
            'Ñ': 'N',
            'á': 'a',
            'é': 'e',
            'í': 'i',
            'ó': 'o',
            'ú': 'u',
            'ü': 'u',
            'ñ': 'n'
          };

      return (this.replace(translate_re, function(match) {
        return translate[match];
      }));
    }
  });

  // Mueve el juego; viene de: http://alturl.com/musku
  function scrollTo(element, to, duration) {
    if (duration <= 0) {
      return;
    }

    var difference = to - element.scrollTop,
        perTick = difference / duration * 10;

    setTimeout(function() {
      element.scrollTop = element.scrollTop + perTick;

      if (element.scrollTop === to) {
        return;
      }

      scrollTo(element, to, duration - 10);
    }, 10);
  }

  // Incrementa la barra de progreso
  function resize_bar(increment = 0) {
    var div_pbar = document.getElementById('foo-bar'),
        total_width = document.body.clientWidth,
        current_width = div_pbar.clientWidth,
        total_lines = game_opt.data.content.length -
        (game_opt.size[1] - 1),
        line_width = total_width / total_lines,
        new_width;

    // El intervalo se cambia con una animación
    function move() {
      if (current_width >= new_width) {
        clearInterval(bar_timer);
        bar_timer = 0;
      } else {
        if (increment) {
          current_width += 10;
        } else {
          current_width++;
        }
        div_pbar.style.width = current_width + 'px';
      }
    }

    // Cálculo de su posición inicial o desde el último guardado
    if (increment == 0) {
      var lines = document.getElementsByClassName('foo-line'),
          last_lt = lines[lines.length - 1].getElementsByClassName('foo-letter')[0].getAttribute('data-status'),
          total = 0;

      // Obtiene cuántas líneas se han avanzado
      for (var i = 0; i < lines.length; i++) {
        if (lines[i].getAttribute('data-visible') == 'true') {
          total += 1;
        }
      }

      // Según si todas las líneas han sido respondidas o no
      if (total_lines == total - (game_opt.size[1] - 1) &&
        (last_lt == 'correct' || last_lt == 'incorrect')) {
        increment = (total - (game_opt.size[1] - 1));
      } else {
        increment = (total - (game_opt.size[1] - 1)) - 1;
      }
    }

    // Cambia el tamaño
    new_width = current_width + (line_width * increment);

    // Cambia el tamaño si no se está moviendo
    if (typeof bar_timer === 'undefined' || bar_timer == 0) {
      bar_timer = setInterval(move, 1);
    }
  }

  // Valida conjuntos
  function validate_array(array, min_length, min_quantity, label) {

    // No procede si no tiene la extensión necesaria
    if (array.length != min_length) {
      throw new Error(label + " array requires only " +
        min_length + " elements.");
    }

    // No procede si el tipo o el tamaño mínimo son erróneos
    for (var i = 0; i < array.length; i++) {
      if (typeof array[i] != 'number' ||
        array[i] % 1 !== 0 ||
        array[i] < min_quantity) {
        throw new Error(label + " array's elements have to be" +
          " integers equal or greater" +
          " than " + min_quantity + ".");
      }
    }
  }

  // Ejecuta la función si se estipuló alguna
  function exec_fn(e) {
    // Si 'e' es texto, se supone que es el nombre de una función
    if (typeof e === 'string' &&
      typeof window[e] === 'function') {
      window[e]();
    } else if (typeof e === 'function') {
      e();
    }
  }

  // Cuando pasa cada segundo del juego
  function new_time(extra = 0) {
    var div_timer = document.getElementById('foo-timer-total'),
        qnt_timer = parseInt(div_timer.innerHTML);

    // Agrega un extra, si lo hay
    if (extra != 0) {
      qnt_timer = qnt_timer + extra;
      // Agrega o quita lo regular
    } else {
      if (game_opt.type == 'story') {
        qnt_timer = qnt_timer + 1;
      } else {
        qnt_timer = qnt_timer - 1;
      }
    }

    // Se fija variable
    game_opt.time = qnt_timer;
    foo.data.time = game_opt.time;

    // Imprime el resultado
    div_timer.innerHTML = qnt_timer;

    // Corre cuando se llegan a los últimos 10 segundos del arcade
    if (qnt_timer == 10 && game_opt.type == 'arcade') {
      exec_fn(game_opt.ar_on_ten_seconds);
    }

    // Si se llega a cero y es arcade, se deshabilita
    if (qnt_timer <= 0 && game_opt.type == 'arcade') {
      lose();
    }

    // Corre función cuando se cambia el temporizador
    exec_fn(game_opt.on_new_time);
  }

  // Cuando hay un cambio en el puntaje
  function new_score(int) {
    var div_score = document.getElementById('foo-score-total'),
        qnt_score = parseInt(div_score.innerHTML);

    // Se fija variable
    game_opt.past_score = qnt_score;
    foo.data.past_score = game_opt.past_score;

    qnt_score = qnt_score + int;

    // Se fija variable
    game_opt.score = qnt_score;
    foo.data.score = game_opt.score;

    // Imprime el resultado
    div_score.innerHTML = qnt_score;

    // Si la puntuación es menor a cero, se deshabilita
    if (qnt_score < 0) {
      lose();
    }

    // Corre función cuando se cambia el puntaje
    exec_fn(game_opt.on_new_score);
  }

  // Cuando empieza el touch
  function on_touchstart(e) {
    // Dibuja la letra actual para mayor visibilización
    function draw_label(l) {
      var d_add = document.getElementById('foo-adds'),
          p_add = document.createElement('p');

      // Elimina el token con la letra presionada
      if (document.getElementById('foo-token')) {
        document.getElementById('foo-token').parentElement
          .removeChild(document.getElementById('foo-token'));
      }

      // Muestra los anuncios
      d_add.style.display = 'inherit';

      // Agrega la etiqueta y sus estilos
      p_add.id = 'foo-token';
      p_add.style
        .cssText = 'width:' + d_add.clientHeight +
        'px;height:' +
        d_add.clientHeight +
        'px;font-size:' +
        ((parseInt(game_opt.game_fontSize) * 2) +
          game_opt.game_fontSize.replace(/\d+/, '')) +
        ';background:' +
        game_opt.lt_bkcolor_enable +
        ';border-radius:' +
        (d_add.clientHeight / 5) +
        'px;margin:0 auto;display:flex;' +
        'align-items:center;justify-content:center;' +
        'border:1px solid #ccc;' +
        '-webkit-box-shadow: ' +
        '0 0 25px 0 rgba(0,0,0,0.25);' +
        '-moz-box-shadow: ' +
        '0 0 25px 0 rgba(0,0,0,0.25);' +
        'box-shadow: 0 0 25px 0 rgba(0,0,0,0.25);';
      p_add.innerHTML = l;
      d_add.appendChild(p_add);
    }

    // Analiza si ejecutar las funciones de la bomba o el bono
    function trigger_bomb_bonus(el, i) {
      var bomb_bonus = game_opt.type + '_bombs_bonus_' + game_opt.lang,
          picked = el.getAttribute('data-picked'),
          fn = picked == 0 ? el.getAttribute('data-on_bomb') : el.getAttribute('data-on_bonus'),
          line_index = get_index(el.parentElement.previousSibling, 0),
          letter_index = get_index(el.previousSibling, 0),
          position = line_index + '-' + letter_index + '-' + i,
          valid = true,
          final_pos = [];

      // Obtiene el índice
      function get_index(e, i) {
        while (e != null) {
          e = e.previousSibling;
          i++;
        }

        return i;
      }

      // Si no hay datos guardados, se guarda
      if (localStorage.getItem(bomb_bonus) == null) {
        final_pos.push(position);
        // Si hay datos, se analiza e incorporan
      } else {
        var bomb_bonus_array = JSON.parse(localStorage[bomb_bonus]);

        // Si ya se activó no será válido
        for (var j = 0; j < bomb_bonus_array.length; j++) {
          if (bomb_bonus_array[j] == position) {
            valid = false;
          }
        }

        bomb_bonus_array.push(position);

        final_pos = bomb_bonus_array.slice();
      }

      /* Si es válido, se ejecuta, cambia la puntuación y guarda
         el registro de las bombas o los bonos activados */
      if (valid) {
        // Corre función cuando hay bomba o bono
        exec_fn(fn);

        // Cambia la puntuación
        if (picked == 0) {
          new_score(-3);
        } else {
          new_score(3);
        }

        // Guardado de los datos
        localStorage.setItem(bomb_bonus, JSON.stringify(final_pos));

        return '<span style="font-size:' +
          game_opt.lt_chance_fontSize +
          ';vertical-align:.25em;">' +
          (picked == '0' ? '-' : '+') +
          '</span>3';
      } else {
        return '<span style="color:#999">' +
          '<span style="font-size:' +
          game_opt.lt_chance_fontSize +
          ';vertical-align:.25em;">' +
          (picked == '0' ? '-' : '+') +
          '</span>3</span>';
      }
    }

    if (!game_opt.win) {
      var element = e.currentTarget,
          p = element.getElementsByTagName('p')[0],
          status = element.getAttribute('data-status'),
          picked = element.getAttribute('data-picked'),
          picked_i = element.getAttribute('data-picked_i'),
          letters = element.getAttribute('data-letters').split('¤'),
          chances = element.getAttribute('data-chances'),
          loops = element.getAttribute('data-loops'),
          on_st_change = element.getAttribute('data-on_first_change'),
          on_change = element.getAttribute('data-on_change'),
          on_loop = element.getAttribute('data-on_loop');

      game_opt.currentTarget = element;

      // Se fijan variables
      game_opt.letter = element;
      foo.data.letter = game_opt.letter;

      // Evita falsos positivos
      clearInterval(touch_timer);

      /* Para dedos rápidos, si se cambió de ficha,
         quita una oportunidad a la ficha antigua */
      if (touch_old != element && touch_old != null) {
        compute_letter(touch_old);
      }

      // Solo si todavía hay oportunidades disponibles
      if (chances > 0 && status == 'enable') {
        // Corre función cuando se está cambiando de letra
        exec_fn(on_change);

        // Corre función cuando es la 1ra vez que se cambia de letra
        if (loops == 0 && picked == letters[0]) {
          exec_fn(on_st_change);
        }

        // Iteración para encontrar la siguiente letra
        for (var i = 0; i < letters.length; i++) {
          if (i == picked_i) {
            var label;

            // Obtiene la siguiente letra
            if (i != letters.length - 1) {
              picked_i = i + 1;
            } else {
              picked_i = 0;
              loops = parseInt(loops) + 1;
              element.setAttribute('data-loops', loops);

              // Corre función cuando se ha dado una vuelta
              exec_fn(on_loop);
            }

            // Cambia la letra elegida
            picked = letters[picked_i];
            element.setAttribute('data-picked', picked);
            element.setAttribute('data-picked_i', picked_i);

            // Etiqueta según si es letra, bomba o bonus
            if (picked != 0 && picked != 1) {
              label = picked;
            } else {
              label = trigger_bomb_bonus(element, i + 1);
            }

            // Cambia la etiqueta
            draw_label(label);
            p.innerHTML = label;

            break;
          }
        }
      }

      // Evita falsos positivos
      touch_old = element;
    }
  }

  // Cuando termina el touch
  function on_touchend(e) {
    var element = e.currentTarget;

    // El usuario tiene cierto tiempo antes de perder una oportunidad
    if (!game_opt.win) {
      // Si el touch empezó en el mismo elemento que finaliza
      if (element == game_opt.currentTarget) {
        touch_timer = setInterval(compute_letter, 500, this);
      } else {
        touch_timer = setInterval(
          compute_letter, 500, game_opt.currentTarget
        );
      }
    }
  }

  // Analiza todo lo relativo a las letras
  function compute_letter(e) {
    var chances = e.getAttribute('data-chances'),
        status = e.getAttribute('data-status'),
        d_add = document.getElementById('foo-adds');

    // Reseteo de varios elementos para evitar falsos positivos
    clearInterval(touch_timer);
    touch_old = null;
    d_add.style.display = 'none';
    if (document.getElementById('foo-token')) {
      document.getElementById('foo-token')
        .parentElement
        .removeChild(document.getElementById('foo-token'));
    }

    /* Cambia la cantidad de oportunidades y si ya se llegó a 0,
       se cambia de estado y color */
    if (chances > 0 && status == 'enable') {
      e.setAttribute('data-chances', chances - 1);
      e.getElementsByTagName('p')[1].innerHTML = chances - 1;

      if (chances - 1 == 0) {
        e.setAttribute('data-status', 'disable');
        e.style.background = game_opt.lt_bkcolor_disable;
        e.style.cssText += '-webkit-user-select:none;' +
          '-moz-user-select:none;' +
          '-ms-user-select:none;' +
          'user-select:none;';

        // Reemplaza para eliminar los disparadores
        var e_new = e.cloneNode(true);

        e.parentNode.replaceChild(e_new, e);
        e = e_new;
      }
    }

    // Analiza si la línea se completó y en cuál condición
    compute_line(e.parentNode);

    // Guarda el estado del juego
    foo.save();
  }

  // Analiza todo lo relativo a las líneas
  function compute_line(e) {
    var foo_game = document.getElementById('foo-game'),
        on_line_completed = e.getAttribute('data-on_line_completed'),
        on_line_correct = e.getAttribute('data-on_line_correct'),
        on_line_incorrect = e.getAttribute('data-on_line_incorrect'),
        childrens = e.childNodes,
        disable = true,
        correct = true,
        next = true,
        score = 0,
        bar_resize = 1,
        results = [],
        line_text = '',
        line_text_correct = '',
        line_completed = (childrens[0].getAttribute('data-status') != 'correct') && (childrens[0].getAttribute('data-status') != 'incorrect');

    // Cambia el color de la letra según esté correcta o no
    function change_color(childrens) {
      for (var k = 0; k < childrens.length; k++) {
        var child = childrens[k],
            child_type = child.getAttribute('data-type'),
            child_correct = child.getAttribute('data-correct'),
            child_picked = child.getAttribute('data-picked');

        if (child_type == '1') {
          if (child_correct == child_picked) {
            child.style.background = game_opt.lt_bkcolor_correct;
            child.setAttribute('data-status', 'correct');
          } else {
            child.style.background = game_opt.lt_bkcolor_incorrect;
            child.setAttribute('data-status', 'incorrect');
          }
        }
      }
    }

    // Se fijan variables
    game_opt.line = e;
    foo.data.line = game_opt.line;

    // Iteración de las letras para determinar el estado de la línea
    for (var i = 0; i < childrens.length; i++) {
      var child = childrens[i],
          child_status = child.getAttribute('data-status'),
          child_correct = child.getAttribute('data-correct'),
          child_picked = child.getAttribute('data-picked'),
          child_chances = child.getAttribute('data-chances');

      // Implica que la línea no tiene todas las letras deshabilitadas
      if (child_status == 'enable') {
        disable = false;
        results.push(child_correct == child_picked ? true : false);
      }

      // Quiere decir que la línea no tiene todas las letras correctas
      if (child_correct != child_picked) {
        correct = false;
        results.push(child_status == 'enable' ? false : true);
      }

      // Va obteniendo la puntuación de la línea
      if (child_chances != null) {
        score += parseInt(child_chances);
      }

      // Obtiene la oración completa
      line_text += (child_picked != null ? child_picked : ' ');

      // Obtiene la línea correcta
      line_text_correct += (child_correct != null ?
        child_correct : ' ');
    }

    // Evita un falso negativo
    for (var j = 0; j < results.length; j++) {
      if (results[j] == false) {
        next = false;
      }
    }

    /* Una línea se considera completada cuando ya no hay letras
       habilitadas o es correcta la elección o mezcla de ambas */
    if (disable || correct || next) {

      // Añade el puntaje
      if (line_completed) {
        new_score(score);
      }

      // Cambia el color de las letras
      change_color(childrens);

      // Se fija variable
      game_opt.line_correct = line_text_correct.trim();
      foo.data.line_correct = game_opt.line_correct;

      // Analiza las palabras en búsqueda de alguna nueva
      compute_words(line_text, line_text_correct);

      // Corre función cuando se completa la línea correctamente
      if (correct) {
        if (line_completed) {

          // Si se trata del modo arcade, además agrega unos segundos
          if (game_opt.type == 'arcade') {
            new_time(score);
          }

          // Se suma una línea correcta
          foo.data.lines_correct++;

          // Corre función cuando se completa la línea correctamente
          exec_fn(on_line_correct);
        }
        // Corre función cuando se completa incorrectamente
      } else {
        if (line_completed) {

          // Se suma una línea correcta
          foo.data.lines_incorrect++;

          // Corre función cuando se completa la línea incorrectamente
          exec_fn(on_line_incorrect);
        }
      }

      // Habilita la siguiente línea o termina el juego
      if (e.nextSibling != null) {
        if (e.nextSibling.childNodes.length == 1) {
          e.nextSibling.setAttribute('data-visible', true);
          e.nextSibling.style.display = 'initial';
          e.nextSibling.nextSibling
            .setAttribute('data-visible', true);
          e.nextSibling.nextSibling.style.display = 'initial';
          bar_resize = 2;
        } else {
          e.nextSibling.setAttribute('data-visible', true);
          e.nextSibling.style.display = 'initial';
        }
      } else {
        if (line_completed) {
          win();
        }
      }

      // Incrementa la barra de progreso
      if (line_completed) {
        resize_bar(bar_resize);
      }

      // Muestra la nueva línea
      scrollTo(foo_game, foo_game.scrollHeight, 500);

      // Corre función cuando se completa la línea
      if (line_completed) {
        exec_fn(on_line_completed);
      }
    }
  }

  // Analiza todo lo relativo a las palabras
  function compute_words(line_text, line_text_correct) {
    var string_w = game_opt.type + '_words_' + game_opt.lang,
        sav_words = localStorage.getItem(string_w) != null ? JSON.parse(localStorage.getItem(string_w)) : {total: game_opt.data.words.length, current: 0, words: []},
        current_w = line_text.replace(/[^(\s|A-zÀ-ÿ|\-)]/, '').split(/\s+/).clear(),
        correct_w = line_text_correct.replace(/[^(\s|A-zÀ-ÿ|\-)]/, '').split(/\s+/).clear();

    // Añade la palabra al guardado local y a 'foo.data'
    function add_word(w) {
      w = w.capitalize();

      // Si la palabra no está en el conjunto
      if (sav_words.words.indexOf(w) == -1) {
        sav_words.words = sav_words.words.concat(w);
        sav_words.current = sav_words.words.length;

        // Se fija variable
        game_opt.new_words = sav_words.words.slice();
        foo.data.new_words = game_opt.new_words.slice();

        // Guarda los datos
        localStorage.setItem(string_w, JSON.stringify(sav_words));

        // Corre función cuando hay una nueva palabra
        exec_fn(game_opt.on_new_word);
      }
    }

    // Analiza la línea palabra por palabra
    for (var i = 0; i < current_w.length; i++) {
      var word = '';

      // Si la palabra es la correcta
      if (current_w[i] == correct_w[i]) {
        // Guarda la palabra si tiene separación silábica
        if (correct_w[i].charAt(correct_w[i].length - 1) == '-') {
          localStorage.setItem('incomplete_word', correct_w[i]);
          // Se completa una palabra si estaba separada
        } else if (
          localStorage.getItem('incomplete_word') != null &&
          localStorage.getItem('incomplete_word') != 'null' &&
          localStorage.getItem('incomplete_word') != '') {
          word = (localStorage.getItem('incomplete_word') +
            correct_w[i]).replace('-', '');
          localStorage.removeItem('incomplete_word');
          // Palabra no interrumpida por separación silábica
        } else if (
          localStorage.getItem('incomplete_word') != 'null') {
          word = correct_w[i];
        }

        // Resetea
        if (localStorage.getItem('incomplete_word') == 'null') {
          localStorage.removeItem('incomplete_word');
        }

        // Añade la palabra
        if (word != '') {
          add_word(word);
        }

        // Si no es correcta, limpia la palabra guardada
      } else {
        /* Cuando era una palabra con separación silábica se marca
           nulo para detectarla y rechazar el resto de la palabra */
        if (correct_w[i].charAt(correct_w[i].length - 1) == '-') {
          localStorage.setItem('incomplete_word', null);
          // Resetea
        } else {
          localStorage.removeItem('incomplete_word');
        }
      }
    }
  }

  // Analiza si hay nuevos récords
  function highest_scores() {
    var hig_score = localStorage.getItem(game_opt.type + '_highest_scores_' + game_opt.lang),
        new_score = foo.data.score,
        new_words = game_opt.new_words;

    // Con esta manera se evitan falsos positivos
    if (typeof new_words !== 'undefined') {

      // Verifica y guarda posibles nuevos récords
      if (hig_score != null) {
        hig_score = JSON.parse(hig_score);
      } else {
        hig_score = [];
      }

      // Indaga si hay un nuevo récord
      if (hig_score.length < 10) {
        exec_fn(game_opt.on_new_record);
      } else {
        if (new_score > hig_score[hig_score.length - 1]) {
          exec_fn(game_opt.on_new_record);
        }
      }

      // Añade el récord
      hig_score.push(new_score);
      hig_score = hig_score.sort().reverse();

      // Disminuye los récords si son más de lo deseado
      if (hig_score.length > game_opt.top) {
        hig_score = hig_score.slice(0, game_opt.top);
      }

      // Guarda los récords
      localStorage.setItem(game_opt.type +
        '_highest_scores_' +
        game_opt.lang, JSON.stringify(hig_score));
    }
  }

  // Cuando se gana el juego
  function win() {
    // Si se reinicia un juego que ya está completo
    if (foo.data.letter == null) {
      foo.data.active = false;
      foo.data.finished = true;

      // Detiene el temporizador
      clearInterval(game_timer);
      // Cuando se gana la primera vez
    } else {
      var additional_score = parseInt(foo.data.time / 4);

      // Incrementa la barra de progreso
      resize_bar(1);

      // Hay un límite de 100 puntos
      if (additional_score > 100) {
        additional_score = 100;
      }

      // En el modo historia se restan puntos relativos al tiempo
      if (game_opt.type == 'story') {
        new_score(-Math.abs(additional_score));
        // En el modo arcade se suman puntos relativos al tiempo
      } else {
        new_score(additional_score);
      }

      // Analiza si hay nuevos récords
      highest_scores();

      // Se fijan variables
      game_opt.finished = true;
      foo.data.finished = game_opt.finished;
      foo.disable();

      // Corre función cuando se completa el juego satisfactoriamente
      exec_fn(game_opt.on_win);
    }

    // Ya no es necesario y puede ser conflictivo
    localStorage.removeItem('incomplete_word');

    game_opt.win = true;
  }

  // Cuando se pierde el juego
  function lose() {
    foo.disable();

    // Corre función cuando se completa el juego insatisfactoriamente
    exec_fn(game_opt.on_lose);

    foo.data.active = false;
    foo.data.finished = true;
  }

  return {
    // Permite acceso a datos del juego
    data: {
      type: null,
      active: null,
      finished: null,
      time: null,
      score: null,
      past_score: null,
      line: null,
      line_correct: null,
      lines_correct: null,
      lines_incorrect: null,
      letter: null,
      new_words: []
    },

    // Crea el juego
    create: function(opt) {
      opt.type =
        opt.type || null;
      opt.url =
        opt.url || null;
      opt.lang =
        opt.lang || 'es';
      opt.size =
        opt.size || [7, 13];
      opt.margin =
        opt.margin || [2, 1, 1, 1];
      opt.border =
        opt.border || [0, 0];
      opt.score_position =
        opt.score_position || 2;
      opt.timer_position =
        opt.timer_position || 0;
      opt.top =
        opt.top || 10;
      opt.score_label =
        opt.score_label || 'Puntos';
      opt.timer_label =
        opt.timer_label || 'Tiempo';
      opt.border_style =
        opt.border_style || 'solid';
      opt.border_color =
        opt.border_color || 'black';
      opt.game_fontSize =
        opt.game_fontSize || '1em';
      opt.game_fontFamily =
        opt.game_fontFamily || 'Impact, Charcoal, sans-serif';
      opt.labels_fontSize =
        opt.labels_fontSize || '.75em';
      opt.labels_fontFamily =
        opt.labels_fontFamily || 'Impact, Charcoal, sans-serif';
      opt.totals_fontSize =
        opt.totals_fontSize || '1.5em';
      opt.totals_fontFamily =
        opt.totals_fontFamily || 'Impact, Charcoal, sans-serif';
      opt.lt_fontSize =
        opt.lt_fontSize || '1em';
      opt.lt_chance_fontSize =
        opt.lt_chance_fontSize || '.5em';
      opt.lt_bkcolor_correct =
        opt.lt_bkcolor_correct || 'green';
      opt.lt_bkcolor_incorrect =
        opt.lt_bkcolor_incorrect || 'red';
      opt.lt_bkcolor_enable =
        opt.lt_bkcolor_enable || 'white';
      opt.lt_bkcolor_disable =
        opt.lt_bkcolor_disable || '#ddd';
      opt.bar_style =
        opt.bar_style || '3px solid #ccc';
      opt.on_init =
        opt.on_init || '';
      opt.on_finish =
        opt.on_finish || '';
      opt.on_win =
        opt.on_win || '';
      opt.on_lose =
        opt.on_lose || '';
      opt.on_error =
        opt.on_error || '';
      opt.on_new_time =
        opt.on_new_time || '';
      opt.on_new_score =
        opt.on_new_score || '';
      opt.on_new_word =
        opt.on_new_word || '';
      opt.on_new_record =
        opt.on_new_record || '';
      opt.ar_chances =
        opt.ar_chances || 3;
      opt.ar_min_answers =
        opt.ar_min_answers || 2;
      opt.ar_max_answers =
        opt.ar_max_answers || 3;
      opt.ar_max_answers_total =
        opt.ar_max_answers_total || 5;
      opt.ar_bomb_or_bonus =
        opt.ar_bomb_or_bonus || 20;
      opt.ar_random_first_letter =
        opt.ar_random_first_letter || 3;
      opt.ar_length =
        opt.ar_length || 10;
      opt.ar_on_ten_seconds =
        opt.ar_on_ten_seconds || '';
      opt.ar_on_line_completed =
        opt.ar_on_line_completed || '';
      opt.ar_on_line_correct =
        opt.ar_on_line_correct || '';
      opt.ar_on_line_incorrect =
        opt.ar_on_line_incorrect || '';
      opt.ar_on_first_change =
        opt.ar_on_first_change || '';
      opt.ar_on_change =
        opt.ar_on_change || '';
      opt.ar_on_loop =
        opt.ar_on_loop || '';
      opt.ar_on_bomb =
        opt.ar_on_bomb || '';
      opt.ar_on_bonus =
        opt.ar_on_bonus || '';

      // Corre función cuando está empezando a estar listo
      exec_fn(opt.on_init);

      // Obtiene la información del JSON
      function request(url) {
        var xhr = new XMLHttpRequest();

        // Inicio de la petición
        xhr.open('GET', url);
        xhr.responseType = 'text';
        xhr.send();

        // Cuando está cargando
        xhr.onprogress = function(e) {
          if (e.lengthComputable) {
            console.log(parseInt((e.loaded / e.total) * 100) + '%');
          } else {
            console.log(lang_loading);
          }
        };

        // Cuando está listo
        xhr.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var result = relabel(JSON.parse(this.responseText));

            // Guardado local del juego
            localStorage.setItem(game_opt.type + '_' +
              game_opt.lang, result);

            // Manda a construir el juego
            build();
          }
        };

        // Cuando hay error
        xhr.onerror = function() {
          // Corre función cuando hay error
          exec_fn(game_opt.on_error);
          throw new Error(lang_load_error);
        };
      }

      // Reelabora los datos de arcade para la estructura necesaria
      function relabel(result) {
        var new_result = {
          type: result.type,
          score: result.score,
          timer: result.timer,
          words: result.words,
          content: (game_opt.type == 'story' ? result.content : [])
        };

        // Obtiene las posibles respuestas
        function random_letters(index_total, index_actual,
          correct, ignore) {
          var total = Math.floor(Math.random() * (game_opt.ar_max_answers - game_opt.ar_min_answers + 1)) + game_opt.ar_min_answers,
              final_pick = [],
              status = 'disable',
              reach;

          // Elige las letras incorrectas
          function random_select(c) {

            // Basada en Scrabble: http://alturl.com/4j8co
            // Español
            window.foo_vowels_es = [
              'a', 'a', 'a', 'a', 'a', // x12
              'a', 'a', 'a', 'á', 'á',
              'á', 'á',
              'e', 'e', 'e', 'e', 'e', // x12
              'e', 'e', 'e', 'é', 'é',
              'é', 'é',
              'i', 'i', 'i', 'i', 'í', // x6
              'í',
              'o', 'o', 'o', 'o', 'o', // x9
              'o', 'ó', 'ó', 'ó',
              'u', 'u', 'u', 'ú', 'ü'
            ]; // x5
            window.foo_consonants_es = [
              'b', 'b', // x2
              'c', 'c', 'c', 'c', // x4
              'd', 'd', 'd', 'd', 'd', // x5
              'f', // x1
              'g', 'g', // x2
              'h', 'h', // x2
              'j', // x1
              'k', // x1
              'l', 'l', 'l', 'l', // x4
              'm', 'm', // x2
              'n', 'n', 'n', 'n', 'n', // x5
              'ñ', // x1
              'p', 'p', // x2
              'q', // x1
              'r', 'r', 'r', 'r', 'r', // x5
              's', 's', 's', 's', 's', // x6
              's',
              't', 't', 't', 't', // x4
              'v', 'w', 'x', 'y', 'z'
            ]; // x1
            window.foo_punctuation_es = [
              '.', '.', '.', '.', '.',
              ',', ',', ',', ',', ',',
              ';', ';', ';', ':', ':',
              ':', '¡', '¡', '¡', '¡',
              '!', '!', '!', '!', '¿',
              '¿', '¿', '¿', '?', '?',
              '?', '?', '(', '(', '(',
              ')', ')', ')', '«', '»',
              '“', '”', '-', '[', ']'
            ];

            // Inglés
            window.foo_vowels_en = [
              'a', 'a', 'a', 'a', 'a', // x9
              'a', 'a', 'a', 'a',
              'e', 'e', 'e', 'e', 'e', // x12
              'e', 'e', 'e', 'e', 'e',
              'e', 'e',
              'i', 'i', 'i', 'i', 'i', // x9
              'i', 'i', 'i', 'i',
              'o', 'o', 'o', 'o', 'o', // x8
              'o', 'o', 'o',
              'u', 'u', 'u', 'u'
            ]; // x4
            window.foo_consonants_en = [
              'b', 'b', // x2
              'c', 'c', // x2
              'd', 'd', 'd', 'd', // x4
              'f', 'f', // x2
              'g', 'g', 'g', // x3
              'h', 'h', // x2
              'j', // x1
              'k', // x1
              'l', 'l', 'l', 'l', // x4
              'm', 'm', // x2
              'n', 'n', 'n', 'n', 'n', // x6
              'n',
              'p', 'p', // x2
              'q', // x1
              'r', 'r', 'r', 'r', 'r', // x6
              'r',
              's', 's', 's', 's', // x4
              't', 't', 't', 't', 't', // x6
              't',
              'v', 'v', // x2
              'w', 'w', // x2
              'x', // x1
              'y', 'y', // x2
              'z'
            ]; // x1
            window.foo_punctuation_en = [
              '.', '.', '.', '.', '.',
              ',', ',', ',', ',', ',',
              ';', ';', ';', ':', ':',
              ':', '’', '’', '’', '’',
              '!', '!', '!', '!', '?',
              '?', '?', '?', '(', '(',
              '(', ')', ')', ')', '“',
              '”', '-', '[', ']'
            ];

            var vowels = window['foo_vowels_' + game_opt.lang],
                consonants = window['foo_consonants_' + game_opt.lang],
                punctuation = window['foo_punctuation_' + game_opt.lang];

            // Cuando aún no hay nada, se agrega la respuesta correcta
            if (final_pick.length == 0) {
              final_pick.push(c);
              // Elección de las letras
            } else {
              var accepted = true,
                  candidate_type = Math.floor(Math.random() * (game_opt.ar_bomb_or_bonus - 0 + 1)) + 0,
                  candidate;

              // Cero es bomba
              if (candidate_type == 0) {
                candidate = 0;
                // Uno es bono
              } else if (candidate_type == 1) {
                candidate = 1;
                // El resto es un caracter
              } else {
                // Detecta si es vocal, consonante o puntuación
                if (/\w/.test(c.transliterate())) {
                  if (/[aeiou]/.test(c.transliterate())) {
                    candidate = vowels.sample();
                  } else {
                    candidate = consonants.sample();
                  }
                } else {
                  candidate = punctuation.sample();
                }
              }

              // Indaga si está el candidato presente en el conjunto
              for (var k = 0; k < final_pick.length; k++) {
                if (candidate_type > 1 &&
                  candidate.toLowerCase() ==
                  ('' + final_pick[k]).toLowerCase()) {
                  accepted = false;
                  break;
                }
              }

              /* Si el candidato no está en el conjunto, es aceptado;
                 de lo contrario se repite la operación */
              if (accepted) {
                // Hace mayúscula si la respuesta correcta lo está
                if (candidate_type > 1 &&
                  /[A-Z]/.test(c.transliterate())) {
                  candidate = candidate.toUpperCase();
                }

                final_pick.push(candidate);
              } else {
                random_select(c);
              }
            }
          }

          // Revisa que el conjunto no empiece con una bomba o un bono
          function check_shuffle(array) {
            if (typeof array[0] === 'number') {
              return check_shuffle(array.shuffle());
            } else {
              return array;
            }
          }

          // Obtiene las posibles respuestas, incluyendo la correcta
          while (total != 0) {
            random_select(correct);
            total -= 1;
          }

          /* Estipula si empezar con la letra correcta basado en
             la posibilidad de aparecer 0 entre
             ar_random_first_letter; la primera letra siempre es
             la correcta */
          if (
            Math.floor(
              Math.random() *
              Math.floor(game_opt.ar_random_first_letter)) != 0 &&
            !ignore
          ) {
            final_pick = check_shuffle(final_pick.shuffle());
            status = 'enable';
          }

          // Aumenta la cantidad de respuestas erróneas
          reach = parseInt(
            (index_actual * game_opt.ar_max_answers_total) /
            index_total);

          if (
            game_opt.ar_max_answers < reach &&
            reach <= game_opt.ar_max_answers_total
          ) {
            // Aumenta el máximo posible
            game_opt.ar_max_answers = reach;
            // Aumenta el mínimo posible
            game_opt.ar_min_answers = game_opt.ar_min_answers + 1;
            // Aumenta la posibilidad de que salga bomba o bonus
            game_opt.ar_bomb_or_bonus = game_opt.ar_bomb_or_bonus - 1;
          }

          return {
            status: status,
            letters: final_pick
          };
        }

        // Se agregan líneas en blanco para que empiece en el fondo
        for (var i = 0; i < game_opt.size[1] - 1; i++) {
          if (game_opt.type != 'story') {
            new_result.content.push({
              visible: true,
              on_line_completed: game_opt.ar_on_line_completed,
              on_line_correct: game_opt.ar_on_line_correct,
              on_line_incorrect: game_opt.ar_on_line_incorrect
            });
            // Se van al inicio del conjunto de la historia
          } else {
            new_result.content.unshift({
              visible: true,
              on_line_completed: game_opt.ar_on_line_completed,
              on_line_correct: game_opt.ar_on_line_correct,
              on_line_incorrect: game_opt.ar_on_line_incorrect
            });
          }
        }

        // Cuando es arcade se necesitan hacer más cosas
        if (game_opt.type != 'story') {
          // Reducción aleatoria de elementos y conversión a objeto
          for (var z = 0; z < game_opt.ar_length; z++) {
            var line = result.content.sample(),
                letters = line.split(''),
                new_line = {
                  visible: new_result.content.length == game_opt.size[1] - 1 ? true : false,
                  on_line_completed: game_opt.ar_on_line_completed,
                  on_line_correct: game_opt.ar_on_line_correct,
                  on_line_incorrect: game_opt.ar_on_line_incorrect,
                  content: []
                },
                content_tmp = [];

            // Iteración de la línea en cada letra
            for (var j = 0; j < letters.length; j++) {
              var letter = letters[j];

              // Si es espacio es un objeto sin propiedades
              if (letter == ' ') {
                content_tmp.push({
                  type: 0
                });
                // Objeto con propiedades
              } else {
                var r_letters = random_letters(game_opt.ar_length - 1, z, letter, (j == 0 ? true : false));

                content_tmp.push({
                  type: 1,
                  status: r_letters.status,
                  correct: letter,
                  picked: r_letters.letters[0],
                  picked_i: 0,
                  letters: r_letters.letters,
                  chances: game_opt.ar_chances,
                  loops: 0,
                  on_first_change: game_opt.ar_on_first_change,
                  on_change: game_opt.ar_on_change,
                  on_loop: game_opt.ar_on_loop,
                  on_bomb: game_opt.ar_on_bomb,
                  on_bonus: game_opt.ar_on_bonus
                });
              }
            }

            // Adición del objeto
            new_line.content = content_tmp;
            new_result.content.push(new_line);
          }
        }

        // Los datos ya están listos para la construcción
        return JSON.stringify(new_result);
      }

      // Construye el juego
      function build_game(parent, block_size) {
        game_opt.data = JSON.parse(localStorage[opt.type +
          '_' + game_opt.lang]);

        // Construye cada letra
        function build_letter(div_parent, letter) {
          var div = document.createElement('div'),
              lt_label;

          div.style.float = 'left';
          div.style.padding = '0';

          if (letter.type == 0) {
            div.style.width = block_size + 'px';
            div.style.height = block_size + 'px';
            div.innerHTML = '<p style="margin:0;padding:0;"></p>';
          } else {
            div.setAttribute('data-type', letter.type);
            div.setAttribute('data-status', letter.status);
            div.setAttribute('data-correct', letter.correct);
            div.setAttribute('data-picked', letter.picked);
            div.setAttribute('data-picked_i', letter.picked_i);
            div.setAttribute('data-letters', letter.letters.join('¤'));
            div.setAttribute('data-chances', letter.chances);
            div.setAttribute('data-loops', letter.loops);
            div.setAttribute('data-on_first_change', letter.on_first_change);
            div.setAttribute('data-on_change', letter.on_change);
            div.setAttribute('data-on_loop', letter.on_loop);
            div.setAttribute('data-on_bomb', letter.on_bomb);
            div.setAttribute('data-on_bonus', letter.on_bonus);
            div.classList.add('foo-letter');
            div.style.position = 'relative';
            div.style.width = (block_size - 4) + 'px';
            div.style.height = (block_size - 4) + 'px';
            div.style.margin = '1px';
            div.style.cursor = 'pointer';
            div.style.border = '1px solid #ccc';
            div.style.borderRadius = (block_size / 5) + 'px';

            // Color según su estado
            if (letter.status == 'correct') {
              l_correct = true;
              div.style.background = game_opt.lt_bkcolor_correct;
            } else if (letter.status == 'incorrect') {
              l_incorrect = true;
              div.style.background = game_opt.lt_bkcolor_incorrect;
            } else if (letter.status == 'enable') {
              div.style.background = game_opt.lt_bkcolor_enable;
            } else if (letter.status == 'disable') {
              div.style.background = game_opt.lt_bkcolor_disable;
            }

            // Texto interior según si es letra, o bomba o bono
            if (letter.picked != 0 && letter.picked != 1) {
              lt_label = letter.picked;
            } else {
              lt_label = '<span style="font-size:' +
                game_opt.lt_chance_fontSize +
                ';vertical-align:.25em;">' +
                (letter.letters[0] == '0' ? '-' : '+') +
                '</span>3';
            }

            // Agrega el texto
            div.innerHTML = '<p style="font-size:' +
              opt.lt_fontSize + ';margin:' +
              (block_size / 10) +
              'px 0 0 0;padding:0;' +
              'text-align:center;' +
              '-webkit-user-select:none;' +
              '-moz-user-select:none;' +
              '-ms-user-select:none;' +
              'user-select:none;">' +
              lt_label + '</p>';
            div.innerHTML += '<p style="font-size:' +
              game_opt.lt_chance_fontSize +
              ';position:absolute;bottom:1px;' +
              'right:5px;margin:0;padding:0;' +
              '-webkit-user-select:none;' +
              '-moz-user-select:none;' +
              '-ms-user-select:none;' +
              'user-select:none;">' +
              letter.chances + '</p>';

            // Añade los disparadores según si soporta touch o no
            if ('ontouchstart' in document.documentElement == true) {
              div.addEventListener("touchstart", on_touchstart);
              div.addEventListener("touchend", on_touchend);
            } else {
              div.addEventListener("mousedown", on_touchstart);
              div.addEventListener("mouseup", on_touchend);
            }
          }

          div_parent.appendChild(div);
        }

        // Construye cada línea
        for (var i = 0; i < game_opt.data.content.length; i++) {
          var line = game_opt.data.content[i],
              div = document.createElement('div');

          div.setAttribute(
            'data-visible', line.visible);
          div.setAttribute(
            'data-on_line_completed', line.on_line_completed);
          div.setAttribute(
            'data-on_line_correct', line.on_line_correct);
          div.setAttribute(
            'data-on_line_incorrect', line.on_line_incorrect);
          div.classList.add('foo-line');
          div.style.width = (parent.offsetWidth - 100) + 'px';
          div.style.height = block_size + 'px';
          div.style.margin = '0';
          div.style.padding = '0';
          div.style.display = line.visible ? 'block' : 'none';
          parent.appendChild(div);

          // Si la línea tiene letras, manda a construirlas
          if (typeof line.content !== 'undefined') {
            var l_correct = false,
                l_incorrect = false;

            for (var j = 0; j < line.content.length; j++) {
              build_letter(div, line.content[j]);
            }

            // Cuenta las líneas correctas o incorrectas
            if (l_correct && !l_incorrect) {
              foo.data.lines_correct++;
            }
            if (l_incorrect) {
              foo.data.lines_incorrect++;
            }
            /* Para obligar que tenga algo y sea posible el efecto
               de empezar desde abajo */
          } else {
            div.innerHTML = '<div style="clear:both;width:' +
              block_size +
              'px;height:' +
              block_size +
              'px;margin:0;padding:0;"></div>';
          }
        }
      }

      // Construye todo lo necesario para el juego
      function build() {

        // Construye los bloques interiores del encabezado
        function build_header(i) {
          var div = document.createElement('div');

          div.id = 'foo-head-' + i;
          div.style.float = 'left';
          div.style.width = '33.3333333333%';
          div.style.height = (opt.margin[0] * block_size) + 'px';
          div_head.appendChild(div);
        }

        // Construye la métrica del puntaje y el temporizador
        function build_metric(div) {
          // Se trata de cuatro párrafos, el 1ro y 2do están vacíos
          function build_p(i) {
            var height, p;

            // Altura según el párrafo
            if (i == 1) { // Etiqueta
              height = (div.clientHeight / 8) * 2;
            } else if (i == 2) { // Cantidad
              height = (div.clientHeight / 8) * 4;
            } else { // Vacíos
              height = (div.clientHeight / 8);
            }

            p = document.createElement('p');
            p.style.display = 'flex';
            p.style.alignItems = 'center';
            p.style.justifyContent = 'center';
            p.style.width = '100%';
            p.style.height = height + 'px';
            p.style.margin = '0';
            p.style.padding = '0';
            p.style.textAlign = 'center';
            p.style.cssText += '-webkit-user-select:none;' +
              '-moz-user-select:none;' +
              '-ms-user-select:none;' +
              'user-select:none;';

            // Si es la etiqueta
            if (i == 1) {
              p.id = div.id + '-label';
              p.style.fontSize = opt.labels_fontSize;
              p.style.fontFamily = opt.labels_fontFamily;
              p.innerHTML =
                div.id.split('-')[1] == 'score' ?
                game_opt.score_label : game_opt.timer_label;
              // Si es la cantidad
            } else if (i == 2) {
              p.id = div.id + '-total';
              p.style.fontSize = opt.totals_fontSize;
              p.style.fontFamily = opt.totals_fontFamily;

              // Se fija variable
              if (div.id.split('-')[1] == 'score') {
                game_opt.score = JSON.parse(localStorage[opt.type + '_' + game_opt.lang])[div.id.split('-')[1]];
                foo.data.score = game_opt.score;
                p.innerHTML = game_opt.score;
                foo.data.new_words = localStorage.getItem(opt.type + '_words_' + game_opt.lang) != null ?
                  JSON.parse(localStorage.getItem(opt.type + '_words_' + game_opt.lang)).words :
                  [];
              } else {
                game_opt.time = JSON.parse(localStorage[opt.type + '_' + game_opt.lang])[div.id.split('-')[1]];
                foo.data.time = game_opt.time;
                p.innerHTML = game_opt.time;
              }
            }

            div.appendChild(p);
          }

          for (var i = 0; i < 4; i++) {
            build_p(i);
          }
        }

        validate_array(opt.size, 2, 3, 'Size');
        validate_array(opt.margin, 4, 1, 'Margin');

        // Arranca el temporizador
        game_timer = setInterval(new_time, 1000);

        // Creación de los div principales
        var div_main = document.createElement('div'),
            div_head = document.createElement('div'),
            div_adds = document.createElement('div'),
            div_scrl = document.createElement('div'),
            div_game = document.createElement('div'),
            div_pbar = document.createElement('div');

        // Total de la medida de la rejilla para el juego
        var total_width = opt.size[0] + opt.margin[1] + opt.margin[3],
            total_height = opt.size[1] + opt.margin[0] + opt.margin[2];

        // El bloque es cuadrado, con la medida del lado más pequeño
        var block_width = parseInt(document.documentElement.clientWidth / total_width),
            block_height = parseInt(document.documentElement.clientHeight / total_height),
            block_size = block_width > block_height ? block_height : block_width;

        /* El body no tiene márgenes, abarca todo el documento y
           desplegará sus hijos de modo centrado */
        document.body.style.width = '100vw';
        document.body.style.height = '100vh';
        document.body.style.display = 'flex';
        document.body.style.alignItems = 'center';
        document.body.style.justifyContent = 'center';
        document.body.style.margin = '0';
        document.body.style.padding = '0';

        // DOM principal con el espacio para el juego y los márgenes
        div_main.id = 'foo-main';
        div_main.style.width = (total_width * block_size) + 'px';
        div_main.style.height = (total_height * block_size) + 'px';
        div_main.style.overflow = 'hidden';
        document.body.appendChild(div_main);

        // DOM del encabezado
        div_head.id = 'foo-head';
        div_head.style.width = (opt.size[0] * block_size) + 'px';
        div_head.style.height = (opt.margin[0] * block_size) + 'px';
        div_head.style.margin = '0 ' + (opt.margin[1] * block_size) +
          'px ' + '0 ' + (opt.margin[3] * block_size) + 'px ';
        div_main.appendChild(div_head);

        // Manda a construir los bloques interiores del encabezado
        for (var i = 0; i < 3; i++) {
          build_header(i);
        }

        /* No procede si las posiciones del puntaje y el temporizador 
           son la misma */
        if (opt.score_position == opt.timer_position) {
          throw new Error(
            "Score position cannot be the same that timer position."
          );
        }

        // Obtiene los bloques para la puntuación y el temporizador
        var div_score = document.getElementById('foo-head-' + opt.score_position),
            div_timer = document.getElementById('foo-head-' + opt.timer_position);

        // Cambia el ID por uno más legible
        div_score.id = 'foo-score';
        div_timer.id = 'foo-timer';

        // Añade el texto para las métricas
        build_metric(div_score);
        build_metric(div_timer);

        // DOM de los anuncios
        div_adds.id = 'foo-adds';
        div_adds.style.position = 'absolute';
        div_adds.style.display = 'none';
        div_adds.style.top =
          (div_head.clientHeight + block_size) + 'px';
        div_adds.style.width =
          (opt.size[0] * block_size) + 'px';
        div_adds.style.height = (2 * block_size) + 'px';
        div_adds.style.left = '50%';
        div_adds.style.marginRight = '-50%';
        div_adds.style.transform = 'translate(-50%, 0)';
        div_adds.style.textAlign = 'center';
        div_adds.style.fontFamily = opt.game_fontFamily;
        div_main.appendChild(div_adds);

        // DOM para poder ocultar el pinche scroll
        div_scrl.id = 'foo-scrl';
        div_scrl.style.width =
          (opt.size[0] * block_size) + 'px';
        div_scrl.style.height =
          (opt.size[1] *
            block_size +
            (opt.border[0] + opt.border[1] + 4)
          ) + 'px';
        div_scrl.style.marginLeft = (block_size) + 'px';
        div_scrl.style.padding = '0';
        div_scrl.style.overflow = 'hidden';
        div_main.appendChild(div_scrl);

        // DOM del juego
        div_game.id = 'foo-game';
        div_game.style.width =
          (opt.size[0] * block_size) + 'px';
        div_game.style.height =
          (opt.size[1] * block_size) + 'px';
        div_game.style.margin =
          (opt.border[0] + 2) + 'px 0 0 0';
        div_game.style.padding = '1px 100px 1px 0';
        div_game.style.overflowY = 'scroll';
        div_game.style.fontSize = game_opt.game_fontSize;
        div_game.style.fontFamily = opt.game_fontFamily;
        div_game.style.borderTop =
          opt.border[0] + 'px ' +
          opt.border_style + ' ' +
          opt.border_color;
        div_game.style.borderBottom =
          opt.border[1] + 'px ' +
          opt.border_style + ' ' +
          opt.border_color;
        div_scrl.appendChild(div_game);

        build_game(div_game, block_size);

        // DOM de la barra de progreso
        div_pbar.id = 'foo-bar';
        div_pbar.style.width = 0;
        div_pbar.style.height = 0;
        div_pbar.style.position = 'absolute';
        div_pbar.style.bottom = 0;
        div_pbar.style.left = 0;
        div_pbar.style.borderTop = opt.bar_style;
        div_main.appendChild(div_pbar);

        resize_bar();

        // Mueve el juego a la última línea activa
        div_game.scrollTop = div_game.scrollHeight;
      }

      // Se fijan variables
      game_opt = opt;
      game_opt.active = true;
      game_opt.finished = false;
      foo.data.type = game_opt.type;
      foo.data.active = game_opt.active;
      foo.data.finished = game_opt.finished;

      // La primera vez obtiene la información del JSON
      if (
        localStorage.getItem(game_opt.type + '_' + game_opt.lang) == null
      ) {

        // No procede si el tipo es erróneo
        if (opt.type != 'story' && opt.type != 'arcade') {
          throw new Error(
            "'type' option can only be 'story' or 'arcade'"
          );
        }

        // No procede si no hay ruta
        if (opt.url == null) {
          throw new Error("'url' option is mandatory.");
        } else {
          request(opt.url);
        }
        // Las veces siguientes solo manda a construir el juego
      } else {
        build();
      }

      // Hay ocasiones donde se carga un juego que ya se perdió
      if (foo.data.score < 0 || foo.data.time < 0) {
        setTimeout(lose, 300);
      }

      // Hay ocasiones donde se carga un juego que ya está acabado
      if (
        typeof game_opt.data !== 'undefined' &&
        typeof game_opt.data.content !== 'undefined' &&
        game_opt.data.content != null
      ) {
        if (
          game_opt.data.content[game_opt.data.content.length - 1]
          .content[0].status == 'correct' ||
          game_opt.data.content[game_opt.data.content.length - 1]
          .content[0].status == 'incorrect'
        ) {
          setTimeout(win, 300);
        }
      }

      // Corre función cuando ya está listo
      exec_fn(opt.on_finish);
    },

    // Destruye el juego
    destroy: function(opt = {}) {
      opt.on_init = opt.on_init || '';
      opt.on_finish = opt.on_finish || '';

      // Corre función cuando está empezando a estar listo
      exec_fn(opt.on_init);

      var foo_els = document.body.childNodes;

      // Elimina todos los elementos de Foo
      for (var i = 0; i < foo_els.length; i++) {
        if (/^foo-/.test(foo_els[i].id)) {
          foo_els[i].parentElement.removeChild(foo_els[i]);
        }
      }

      // Resetea el body
      document.body.style.width = '';
      document.body.style.height = '';
      document.body.style.display = '';
      document.body.style.alignItems = '';
      document.body.style.justifyContent = '';
      document.body.style.margin = '';
      document.body.style.padding = '';

      // Se fija variable
      game_opt.active = false;
      foo.data.active = game_opt.active;

      // Detiene el temporizador
      clearInterval(game_timer);

      // Corre función cuando ya está listo
      exec_fn(opt.on_finish);
    },

    // Pausa el juego
    disable: function(opt = {}) {
      opt.on_init = opt.on_init || '';
      opt.on_finish = opt.on_finish || '';

      // Corre función cuando está empezando a estar listo
      exec_fn(opt.on_init);

      if (
        foo.data.active && document.getElementById('foo-main') != null
      ) {
        var foo_disable = document.createElement('div');

        // Se crea una capa que oscurece y evita hacer touches
        foo_disable.id = 'foo-disable';
        foo_disable.style.position = 'absolute';
        foo_disable.style.zIndex = 1;
        foo_disable.style.top = 0;
        foo_disable.style.left = 0;
        foo_disable.style.width = '100vw';
        foo_disable.style.height = '100vh';
        foo_disable.style.background = 'rgba(0,0,0,0.5)';
        document.getElementById('foo-main').appendChild(foo_disable);

        // Se fija variable
        game_opt.active = false;
        foo.data.active = game_opt.active;

        // Detiene el temporizador
        clearInterval(game_timer);
      }

      // Corre función cuando ya está listo
      exec_fn(opt.on_finish);
    },

    // Reanuda el juego
    enable: function(opt = {}) {
      opt.on_init = opt.on_init || '';
      opt.on_finish = opt.on_finish || '';

      // Corre función cuando está empezando a estar listo
      exec_fn(opt.on_init);

      if (!foo.data.active) {
        var foo_disable = document.getElementById('foo-disable');

        // Elimina la pantalla que oscurece el juego
        if (foo_disable != null) {
          foo_disable.parentElement.removeChild(foo_disable);
        }

        // Se fija variable
        game_opt.active = true;
        foo.data.active = game_opt.active;

        // Arranca el temporizador
        game_timer = setInterval(new_time, 1000);
      }

      // Corre función cuando ya está listo
      exec_fn(opt.on_finish);
    },

    // Guarda los datos del juego
    save: function(opt = {}) {
      opt.on_init = opt.on_init || '';
      opt.on_finish = opt.on_finish || '';

      // Corre función cuando está empezando a estar listo
      exec_fn(opt.on_init);

      // Obtiene los elementos generales del juego
      var foo_game = document.getElementById('foo-game'),
          json_game = {
            type: game_opt.type,
            score: parseInt(game_opt.score),
            timer: parseInt(game_opt.time),
            content: []
          };

      // Iteración para obtener los elementos de cada línea
      for (var i = 0; i < foo_game.childNodes.length; i++) {
        var line = foo_game.childNodes[i],
            l_visible =
            line.getAttribute('data-visible'),
            l_on_line_completed =
            line.getAttribute('data-on_line_completed'),
            l_on_line_correct =
            line.getAttribute('data-on_line_correct'),
            l_on_line_incorrect =
            line.getAttribute('data-on_line_incorrect'),
            json_line = {
              visible: (l_visible == 'true'),
              on_line_completed: l_on_line_completed,
              on_line_correct: l_on_line_correct,
              on_line_incorrect: l_on_line_incorrect
            };

        // Si la línea tiene contenido a guardar
        if (line.childNodes[0].getAttribute('data-type') != null) {
          json_line.content = [];

          // Iteración para obtener los elementos de cada letra
          for (var j = 0; j < line.childNodes.length; j++) {
            var letter = line.childNodes[j],
                lt_type = letter.getAttribute('data-type'),
                lt_status = letter.getAttribute('data-status'),
                lt_correct = letter.getAttribute('data-correct'),
                lt_picked = letter.getAttribute('data-picked'),
                lt_picked_i = letter.getAttribute('data-picked_i'),
                lt_letters = letter.getAttribute('data-letters'),
                lt_chances = letter.getAttribute('data-chances'),
                lt_loops = letter.getAttribute('data-loops'),
                lt_on_first_change = letter.getAttribute('data-on_first_change'),
                lt_on_change = letter.getAttribute('data-on_change'),
                lt_on_loop = letter.getAttribute('data-on_loop'),
                lt_on_bomb = letter.getAttribute('data-on_bomb'),
                lt_on_bonus = letter.getAttribute('data-on_bonus'),
                json_letter = {};

            // Si es una letra
            if (parseInt(lt_type) == 1) {
              json_letter = {
                type: parseInt(lt_type),
                status: lt_status,
                correct: lt_correct,
                picked: lt_picked,
                picked_i: parseInt(lt_picked_i),
                letters: lt_letters.split('¤'),
                chances: parseInt(lt_chances),
                loops: parseInt(lt_loops),
                on_first_change: lt_on_first_change,
                on_change: lt_on_change,
                on_loop: lt_on_loop,
                on_bomb: lt_on_bomb,
                on_bonus: lt_on_bonus
              };
              // Si es un espacio
            } else {
              json_letter = {
                type: 0
              };
            }

            // Guarda las letras en la línea
            json_line.content.push(json_letter);
          }
        }

        // Guarda las líneas en el juego
        json_game.content.push(json_line);
      }

      // Guardado local del juego
      localStorage.setItem(
        game_opt.type + '_' + game_opt.lang, JSON.stringify(json_game)
      );

      // Corre función cuando ya está listo
      exec_fn(opt.on_finish);
    },

    // Elimina los datos del juego
    wipe: function(opt = '') {

      // Resetea 'foo.data'
      function wipe_data() {
        foo.data = {
          type: null,
          active: null,
          finished: null,
          time: null,
          score: null,
          past_score: null,
          line: null,
          line_correct: null,
          lines_correct: null,
          lines_incorrect: null,
          letter: null,
          new_words: []
        };
      }

      // Borra elementos seleccionados
      function wipe_me(o, all = false) {
        // Si el juego está activo, lo destruye y resetea
        if (document.getElementById('foo-main')) {
          foo.destroy();
          wipe_data();
          game_opt.data = null;
        }

        // Elimina el juego y la lista de bombas y bonos activados
        localStorage.removeItem(o + '_' + game_opt.lang);
        localStorage.removeItem(o + '_bombs_bonus_' + game_opt.lang);

        // Con esto se borra todo lo relacionado al juego
        if (all) {
          localStorage.removeItem(o + '_words_' + game_opt.lang);
          localStorage.removeItem(o + '_highest_scores_' + game_opt.lang);
        }
      }

      if (opt == '') {
        throw new Error(
          "Function 'wipe' requires 'story' or 'arcade' as parameter."
        );
      }

      // Borra todo o ambas modalidades
      if (opt == 'all' || opt == 'both') {
        if (opt == 'all') {
          wipe_me('story', true);
          wipe_me('arcade', true);
        } else {
          wipe_me('story');
          wipe_me('arcade');
        }
        // Borra alguna modalidad
      } else if (opt == 'story' || opt == 'arcade') {
        wipe_me(opt);
        // Borra 'foo.data'
      } else if (opt == 'data') {
        wipe_data();
      }
    }
  };
})();
